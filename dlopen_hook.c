#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef void* (*dlopen_t) (const char *__file, int __mode);

void* dlopen(const char *file, int mode) {
    static dlopen_t real_dlopen = 0;
    //fprintf(stderr, "called dlopen %s\n", file);
    if (!real_dlopen) {
        real_dlopen = dlsym(RTLD_NEXT, "dlopen");
    }
    if (!real_dlopen) {
        fprintf(stderr, "fatal error: original dlopen not found\n");
        abort();
    }
    int subst = 0;
    if (file) {
        if ((!strcmp(file, "/usr/lib/firefox/libmozavcodec.so")) || (!strcmp(file, "libavcodec.so.58"))) {
            fprintf(stderr, "substituting libavcodec: %s\n", file);
            file = "avcodec_capture_hook.so";
            mode |= RTLD_DEEPBIND;
            subst = 1;
        }
    }
    void* r = real_dlopen(file, mode);
    if (subst && !r) {
        fprintf(stderr, "hooked dlopen returned null: %s\n", dlerror());
    }
    //fprintf(stderr, "hooked dlopen done, returned %p\n", r);
    return r;
}
