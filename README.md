# avcodec_capture
LD_PRELOAD hook to capture audio & video streams from applications using libavcodec (FFmpeg) for decoding.

Use cases:
* record internet radio without transcoding while listening to it
  * but if you can find stream URL (hint: inspector), VLC or ffmpeg can do this and are probably simpler to use
* record WebRTC conferences without transcoding
* for developers: example of LD_PRELOAD hook also working with dlopen/dlsym
* for developers: base for other media manipulation hooks

Doesn't work for:
* ripping Widevine-DRM-protected content - I've tried but appearently decrypted video stream doesn't go through libavcodec
  * if you want to try, be aware that it will probably violate streaming service's ToS and is illegal in some countries

# Building
You'll need libavcodec & libavformat installed in your system library directory (or somewhere else if you hack around with linker arguments and LD_LIBRARY_PATH).
```
$ make
```

# Using

## with application dynamically linked with libavcodec
```
$ MEDIA_CAPTURE_DIR=$HOME/captured_media LD_PRELOAD=$PWD/avcodec_capture_hook.so yourapplication --arguments
```

## with application using dlopen & dlsym for accessing libavcodec functionality
```
$ MEDIA_CAPTURE_DIR=$HOME/captured_media LD_PRELOAD=$PWD/dlopen_hook.so LD_LIBRARY_PATH=$PWD yourapplication --arguments
```

## with Firefox
```
$ ./run_firefox
```
Do not go to shady websites, as sandboxing of the decoding process is disabled to allow writing to the file system.

Firefox uses libavcodec only for decoding MP3, FLAC, AAC, VP8 & VP9 ([source](https://github.com/bolucat/Firefox/blob/master/media/ffvpx/README_MOZILLA) + AAC determinated by own research). So H264 (?), Opus and Vorbis streams won't be captured.

To capture VP8/VP9 video from WebRTC, `media.navigator.mediadatadecoder_vpx_enabled` needs to be set to `true` (in `about:config`).

If you want to download media files from websites, there are probably better ways, such as [youtube-dl](https://youtube-dl.org/).


# Environment variables

* `MEDIA_CAPTURE_DIR` - directory where audio & video streams will be saved, must be specified and must exist
* `DEFAULT_VIDEO_TB` - time base the application is using for video timestamps, e.g. 1/1000000 for microseconds
* `MEDIA_CAPTURE_RAW` - set to non-empty string to disable missing timestamp generation and output format guessing (default .nut will be used for all streams)

# Known bugs

If the application is using deprecated FFmpeg decoding API (avcodec_decode_audio4 & avcodec_decode_video2) and decoder consumes packets partially, the same packet may be written to the output file multiple times.

There is possible but extremally unlikely race condition which may result in corrupted output file if multiple decoders are opened simultaneously (within the same microsecond).
