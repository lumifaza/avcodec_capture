build: dlopen_hook.so avcodec_capture_hook.so

clean:
	rm dlopen_hook.so avcodec_capture_hook.so || true

dlopen_hook.so: dlopen_hook.c
	gcc -shared -fPIC -ldl $< -o $@

avcodec_capture_hook.so: avcodec_capture_hook.cpp
	g++ -Wall -fPIC -g $< -c -o avcodec_capture_hook.o
	gcc -shared avcodec_capture_hook.o -o $@ -ldl -lavcodec -lavformat
