extern "C" {
#include <dlfcn.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/parseutils.h>
#include <libavutil/dict.h>
#include <sys/time.h>
#include <sys/syscall.h>
#include <sys/unistd.h>
}

#include <atomic>
#include <mutex>
#include <unordered_map>
#include <list>


#define DISABLE_DEPRECATION_WARNINGS _Pragma("GCC diagnostic push") _Pragma("GCC diagnostic ignored \"-Wdeprecated-declarations\"")
#define ENABLE_DEPRECATION_WARNINGS  _Pragma("GCC diagnostic pop")

#define myprintf(...) fprintf(stderr, "avcodec_capture: " __VA_ARGS__)

// INTERNAL SYMBOLS BEGIN
namespace {

decltype(avcodec_open2)* real_avcodec_open2;
decltype(avcodec_send_packet)* real_avcodec_send_packet;
decltype(avcodec_close)* real_avcodec_close;
decltype(avcodec_receive_frame)* real_avcodec_receive_frame;
DISABLE_DEPRECATION_WARNINGS
decltype(avcodec_decode_audio4)* real_avcodec_decode_audio4;
decltype(avcodec_decode_video2)* real_avcodec_decode_video2;
ENABLE_DEPRECATION_WARNINGS

char* capture_dir;
bool dump_raw = false;
bool overwrite_dtses = false;
AVRational default_video_tb = {0, 0};

struct OutputFormatInfo {
    const char* suffix;
    AVDictionary* options;
};

OutputFormatInfo get_output_info_by_codec_name(const char* codec_name) {
    #define EQ(name) (!strcmp(codec_name, name))
    if (!dump_raw && codec_name) {
        if (EQ("mp3") || EQ("mp3float")) {
            return { ".mp3", nullptr };
        } else if (EQ("flac")) {
            AVDictionary* opts = nullptr;
            av_dict_set(&opts, "metadata_header_padding", "8192", 0);
            return { ".flac", opts };
        } else if (EQ("aac")) {
            return { ".m4a", nullptr };
        } else if (/*EQ("vp8") || EQ("libvpx") || EQ("vp9") || EQ("libvpx-vp9") ||*/ EQ("vorbis") || EQ("libvorbis") || EQ("opus") || EQ("libopus")) {
            // video formats disabled as webm container enforces timebase which would break DEFAULT_VIDEO_TB
            return { ".webm", nullptr };
        }
    }
    return { ".nut", nullptr };
    #undef EQ
}

size_t current_time(char* buff, size_t bufsize) {
    timeval tv;
    gettimeofday(&tv, NULL);
    size_t len = strftime(buff, bufsize, "%Y-%m-%d_%H-%M-%S", localtime(&tv.tv_sec));
    int ret = snprintf(buff + len, bufsize - len, ".%06lu", tv.tv_usec);
    if (ret>0) {
        buff[bufsize-1] = 0;
        return len + ret;
    } else {
        buff[0] = 0;
        myprintf("current_time failed\n");
        return 0;
    }
}

enum CaptureState {
    CLOSED,  // initial state, when codec has been opened but avcodec hasn't decoded anything. output file is closed.
    DECODED, // something has been decoded - we have metadata available in AVCodecContext
    OPENED,  // output file has been opened but nothing has been written yet
    WRITING,
    DISABLED // unrecoverable error occurred, capture disabled for this stream
};

struct Capture {
    AVFormatContext* ofmt_ctx = nullptr;
    AVCodecContext* codec_ctx = nullptr;
    CaptureState state = CLOSED;
    int64_t next_timestamp = 0;
    int64_t max_pts_dts_diff = 0;
    int64_t prev_dts = -1;
    std::list<AVPacket*> queue;
    bool warned_timestamps = false;
    Capture(AVCodecContext* ctx): codec_ctx(ctx) {
    }
    void open() {
        char timestr[64];
        // TODO: possible but unlikely race condition: microseconds assumed to be unique
        current_time(timestr, 64);
        OutputFormatInfo out_info = get_output_info_by_codec_name(codec_ctx->codec->name);
        char out_filename[strlen(capture_dir) + strlen(timestr) + strlen(out_info.suffix) + 2];
        sprintf(out_filename, "%s/%s%s", capture_dir, timestr, out_info.suffix);
        ofmt_ctx = nullptr;
        //myprintf("before avformat_alloc_output_context2\n");
        avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, out_filename);
        //myprintf("after avformat_alloc_output_context2\n");
        AVStream *out_stream = avformat_new_stream(ofmt_ctx, nullptr);
        if (!out_stream) {
            bail(AVERROR_UNKNOWN, "failed to create output stream");
            return;
        }
        myprintf("before copy: timebase is: codec %i/%i, output stream %i/%i\n",
            codec_ctx->pkt_timebase.num, codec_ctx->pkt_timebase.den, out_stream->time_base.num, out_stream->time_base.den);
        int ret = avcodec_parameters_from_context(out_stream->codecpar, codec_ctx);
        myprintf("after copy: timebase is: codec %i/%i, output stream %i/%i\n",
            codec_ctx->pkt_timebase.num, codec_ctx->pkt_timebase.den, out_stream->time_base.num, out_stream->time_base.den);
        if ((!out_stream->time_base.num || !out_stream->time_base.den) && codec_ctx->codec_type==AVMEDIA_TYPE_VIDEO) {
            myprintf("using default time base %i/%i (set env var DEFAULT_VIDEO_TB to change)\n", default_video_tb.num, default_video_tb.den);
            out_stream->time_base = default_video_tb;
        }
        if (out_stream->time_base.num) {
            max_pts_dts_diff = 10 * out_stream->time_base.den / out_stream->time_base.num;
        }
        if (ret<0) {
            bail(ret, "failed to copy avcodec parameters");
            return;
        }
        av_dump_format(ofmt_ctx, 0, out_filename, 1);
        if (!(ofmt_ctx->oformat->flags & AVFMT_NOFILE)) {
            ret = avio_open(&ofmt_ctx->pb, out_filename, AVIO_FLAG_WRITE);
            if (ret < 0) {
                bail(ret, "failed to open output file");
                return;
            }
        }
        ret = avformat_write_header(ofmt_ctx, &out_info.options);
        if (ret < 0) {
            bail(ret, "failed to write header");
            return;
        }
        myprintf("after open: timebase is: codec %i/%i, output stream %i/%i\n",
            codec_ctx->pkt_timebase.num, codec_ctx->pkt_timebase.den, out_stream->time_base.num, out_stream->time_base.den);
        state = OPENED;
        warned_timestamps = false;
    }
    void gotPacket(const AVPacket *in_pkt) {
        if (state==DISABLED) return;
        AVPacket* packet = av_packet_clone(in_pkt);
        if (state==CLOSED) {
            queue.push_back(packet);
            return;
        }
        writeQueue();
        bool consumed = writePacket(packet);
        if (!consumed) {
            queue.push_back(packet);
        }
    }
    void writeQueue() {
        while (!queue.empty()) {
            bool consumed = writePacket(queue.front());
            if (consumed) {
                queue.pop_front();
            }
        }
    }
    void fixTimestamps(AVPacket *packet) {
        if (dump_raw) return;
        if (packet->pts != AV_NOPTS_VALUE) {
            bool regen = false;
            if (overwrite_dtses) {
                regen = true;
            } else if (packet->dts == AV_NOPTS_VALUE) {
                if (!warned_timestamps) myprintf("have pts but no dts, will fake some\n");
                regen = true;
            } else {
                int64_t diff = packet->pts - packet->dts;
                if (diff < 0) {
                    if (!warned_timestamps)  myprintf("pts < dts, will regenerate dts\n");
                    regen = true;
                } else if (diff > max_pts_dts_diff) {
                    if (!warned_timestamps) myprintf("pts >>> dts, will regenerate dts\n");
                    regen = true;
                }
            }
            if (regen) {
                packet->dts = packet->pts; // FIXME: assuming no B-frames
                warned_timestamps = true;
            }
        }
        if ((packet->dts == AV_NOPTS_VALUE) || (packet->pts == AV_NOPTS_VALUE)) {
            if (!warned_timestamps) {
                if (packet->pts != AV_NOPTS_VALUE) {
                    myprintf("have only pts but no dts, overwriting timestamps anyway\n");
                    myprintf("BUG\n"); // should be handled by above condition
                } else if (packet->dts != AV_NOPTS_VALUE) {
                    myprintf("have only dts but no pts, overwriting timestamps anyway\n"); // FIXME
                } else {
                    myprintf("this stream has no timestamps\n");
                }
                warned_timestamps = true;
            }
            packet->dts = next_timestamp;
            packet->pts = next_timestamp;
        }
    }
    bool writePacket(AVPacket *packet) {
        if (state==DISABLED) return true;
        if (!(state==OPENED || state==WRITING)) {
            open();
        }
        packet->stream_index = 0;
        fixTimestamps(packet);
        //myprintf("before av_write_frame\n");
        int ret = av_write_frame(ofmt_ctx, packet);
        //myprintf("after av_write_frame\n");
        bool was_first = state==OPENED;
        if (ret < 0) {
            if (was_first) {
                show_error(ret, "writing first packet failed (discarding this packet)");
            } else {
                bail(ret, "writing packet failed (will retry in new file)", false);
                return false;
            }
        }
        state = WRITING;
        next_timestamp = std::max(packet->pts, packet->dts) + packet->duration;
        prev_dts = packet->dts;
        //myprintf("packet duration %" PRId64 ", next timestamp % " PRId64 "\n", packet->duration, next_timestamp);
        av_packet_unref(packet);
        return true;
    }
    void decoded() {
        if (state==CLOSED) {
            state = DECODED;
        }
    }
    void show_error(int ret, const char* why) {
        char what[AV_ERROR_MAX_STRING_SIZE] = { 0 };
        av_make_error_string(what, AV_ERROR_MAX_STRING_SIZE, ret);
        myprintf("%s: %s\n", why, what);
    }
    void bail(int ret, const char* why, bool fatal = true) {
        show_error(ret, why);
        close(false);
        if (fatal) {
            state = DISABLED;
            myprintf("this stream won't be captured anymore\n");
        }
    }
    void close(bool flush = true) {
        if (flush && (state==DECODED || state==OPENED || state==WRITING)) {
            writeQueue();
        }
        if (state==OPENED || state==WRITING) {
            av_write_trailer(ofmt_ctx);
        }
        if (ofmt_ctx && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE)) {
            avio_closep(&ofmt_ctx->pb);
        }
        avformat_free_context(ofmt_ctx);
        ofmt_ctx = nullptr;
        state = CLOSED;
    }
};

std::atomic_bool got_symbols{false};
std::mutex getting_symbols;
std::unordered_map<AVCodecContext*, Capture> captures;
std::mutex captures_mutex;

Capture* find_capture_by_context(AVCodecContext* avctx) {
    std::lock_guard<decltype(captures_mutex)> lock(captures_mutex);
    auto it = captures.find(avctx);
    if (it != captures.end()) {
        return &it->second;
    }
    return nullptr;
}

void get_symbols() {
    if (!got_symbols) {
        std::lock_guard<decltype(getting_symbols)> lock(getting_symbols);
        if (got_symbols) return;
        real_avcodec_open2 = (decltype(avcodec_open2)*) dlsym(RTLD_NEXT, "avcodec_open2");
        real_avcodec_send_packet = (decltype(avcodec_send_packet)*) dlsym(RTLD_NEXT, "avcodec_send_packet");
        real_avcodec_receive_frame = (decltype(avcodec_receive_frame)*) dlsym(RTLD_NEXT, "avcodec_receive_frame");
        real_avcodec_close = (decltype(avcodec_close)*) dlsym(RTLD_NEXT, "avcodec_close");
        DISABLE_DEPRECATION_WARNINGS
        real_avcodec_decode_audio4 = (decltype(avcodec_decode_audio4)*) dlsym(RTLD_NEXT, "avcodec_decode_audio4");
        real_avcodec_decode_video2 = (decltype(avcodec_decode_video2)*) dlsym(RTLD_NEXT, "avcodec_decode_video2");
        ENABLE_DEPRECATION_WARNINGS
        if (real_avcodec_open2 && real_avcodec_send_packet && real_avcodec_receive_frame && real_avcodec_close &&
            real_avcodec_decode_audio4 && real_avcodec_decode_video2) {
            got_symbols = true;
        } else {
            myprintf("fatal error: getting symbols failed\n");
            abort();
        }

        capture_dir = getenv("MEDIA_CAPTURE_DIR");
        if (capture_dir && (capture_dir[0]==0)) {
            capture_dir = nullptr;
        }
        if (capture_dir) {
            myprintf("media will be captured to directory %s\n", capture_dir);
        } else {
            myprintf("media won't be captured (MEDIA_CAPTURE_DIR not set or empty). remove LD_PRELOAD if you no longer need this feature.\n");
        }

        const char* tbstr = getenv("DEFAULT_VIDEO_TB");
        if (tbstr) {
            if (av_parse_ratio(&default_video_tb, tbstr, 1<<24, AV_LOG_MAX_OFFSET, nullptr) < 0) {
                myprintf("invalid value of DEFAULT_VIDEO_TB, not setting default time base\n");
            }
        }

        const char* raw_str = getenv("MEDIA_CAPTURE_RAW");
        dump_raw = (raw_str && raw_str[0]);

        const char* overwrite_dts_str = getenv("OVERWRITE_DTS");
        overwrite_dtses = (overwrite_dts_str && overwrite_dts_str[0]);
    }
}

}
// INTERNAL SYMBOLS END

extern "C" int avcodec_open2(AVCodecContext *avctx, const AVCodec *codec, AVDictionary **options) {
    myprintf("called avcodec_open2 with %s codec\n", codec ? (codec->name ? codec->name : "null name") : "null");
    get_symbols();
    int r = real_avcodec_open2(avctx, codec, options);

    if (capture_dir) {
        std::lock_guard<decltype(captures_mutex)> lock(captures_mutex);
        if (captures.count(avctx)==0) {
            captures.emplace(avctx, avctx);
        } else {
            myprintf("avcodec_open2 called for already open codec context\n");
        }
    }

    return r;
}

extern "C" int avcodec_send_packet(AVCodecContext *avctx, const AVPacket *packet) {
    //myprintf("avcodec_send_packet %ld\n", syscall(SYS_gettid));
    get_symbols();
    if (capture_dir) {
        Capture* capture = find_capture_by_context(avctx);
        if (capture) {
            capture->gotPacket(packet);
        } else {
            myprintf("avcodec_send_packet called for unopened codec context\n");
        }
    }
    return real_avcodec_send_packet(avctx, packet);
}

extern "C" int avcodec_receive_frame(AVCodecContext *avctx, AVFrame *frame) {
    //myprintf("avcodec_receive_frame %ld\n", syscall(SYS_gettid));
    get_symbols();
    int r = real_avcodec_receive_frame(avctx, frame);
    if ((r==0) && capture_dir) {
        Capture* capture = find_capture_by_context(avctx);
        if (capture) {
            capture->decoded();
        } else {
            myprintf("avcodec_receive_frame called for unopened codec context\n");
        }
    }
    return r;
}

extern "C" int avcodec_close(AVCodecContext *avctx) {
    myprintf("called avcodec_close\n");
    get_symbols();
    if (capture_dir) {
        std::lock_guard<decltype(captures_mutex)> lock(captures_mutex);
        auto it = captures.find(avctx);
        if (it != captures.end()) {
            Capture capture = it->second;
            capture.close();
            captures.erase(it);
            // use-after-free possible if API is used really wrongly and avcodec_close is called before avcodec_send_packet returns
        } else {
            myprintf("avcodec_closed called for unopened codec context\n");
        }
    }
    return real_avcodec_close(avctx);
}

namespace {

void old_api_common(AVCodecContext *avctx, const AVPacket *avpkt, int result, bool decoded) {
    if (capture_dir) {
        Capture* capture = find_capture_by_context(avctx);
        if (capture) {
            capture->gotPacket(avpkt);
            if (decoded) {
                capture->decoded();
                if (avpkt && (result>0) && (result != avpkt->size)) {
                    myprintf("UNIMPLEMENTED: codec consuming only part of packet, output file will be corrupted!\n");
                }
            }
        } else {
            myprintf("avcodec_decode_audio4 called for unopened codec context\n");
        }
    }
}

}

extern "C" int avcodec_decode_audio4(AVCodecContext *avctx, AVFrame *frame, int *got_frame_ptr, const AVPacket *avpkt) {
    //myprintf("avcodec_decode_audio4 %ld\n", syscall(SYS_gettid));
    get_symbols();
    int r = real_avcodec_decode_audio4(avctx, frame, got_frame_ptr, avpkt);
    old_api_common(avctx, avpkt, r, *got_frame_ptr);
    return r;
}

extern "C" int avcodec_decode_video2(AVCodecContext *avctx, AVFrame *picture, int *got_picture_ptr, const AVPacket *avpkt) {
    //myprintf("avcodec_decode_video2 %ld\n", syscall(SYS_gettid));
    get_symbols();
    int r = real_avcodec_decode_video2(avctx, picture, got_picture_ptr, avpkt);
    old_api_common(avctx, avpkt, r, *got_picture_ptr);
    return r;
}

